from curses import wrapper, curs_set
from typing import List
from time import sleep


class Canvas:
    """Class for drawing text art to the console"""

    def __init__(self):
        wrapper(self.__set_window)
        self.configure_window()
        self.__height, self.__width = self.__window.getmaxyx()
        self.__frame_size = 1
        self.frame_time = self.__frame_size * 0.05

    def __set_window(self, window):
        """Setter for window"""

        self.__window = window

    def configure_window(self):
        # hide cursor
        curs_set(False)

        # allow scrolling
        self.__window.idlok(True)
        self.__window.scrollok(True)

    @property
    def width(self) -> int:
        return self.__width

    @property
    def height(self) -> int:
        return self.__height

    def unexpected_cleanup(self, err: BaseException):
        self.__window.clear()
        self.__window.refresh()
        curs_set(True)
        sleep(self.frame_time)
        exit(err)

    def draw(self, art: List[str]):
        """Draws the given art horizontally flipped

        The art is printed to the console from left to right.
        """

        try:
            frame_count = 0
            completed_rows = set([])
            total_rows = len(art)

            # print the art to the console
            while len(completed_rows) < total_rows:
                # print a frame to the console consisting of a frame_size worth of characters from each row
                for row, value in enumerate(art):
                    start = frame_count * self.__frame_size
                    end = start + self.__frame_size
                    chunk = value[start:end][::-1]
                    frame = self.flip_chars(chunk)

                    # after all characters in row have been printed, insert blank spaces to push out old row data
                    if (start >= len(value)):
                        frame = ' ' * self.__frame_size
                        completed_rows.add(row)

                    # draw the frame if this row is within range
                    try:
                        self.draw_text(row, frame)
                    except:
                        pass

                frame_count += 1
                self.__window.refresh()
                sleep(self.frame_time)

            self.clear_canvas(total_rows)
        except KeyboardInterrupt as e:
            self.unexpected_cleanup(e)

    def draw_text(self, row: int, text: str, length: int = 0):
        """Draws the provided text on screen where specified

        Text is drawn at the location of the provided row and length. 
        Row represents the y-axis and length is the x-axis. If length
        is not provided, then the entire text is drawn starting from
        the row position.
        """

        try:
            # return error if row is out of scope
            if row >= self.__height:
                raise Exception(
                    f"""row {row} exceeds the window's height {self.__height}""")
        except KeyboardInterrupt as e:
            self.unexpected_cleanup(e)

        self.__window.insstr(row, 0, text, length)

    def clear_canvas(self, total_rows: int):
        """Push off the remaining on-screen characters"""

        # append blanks spaces until all on-screen characters are removed
        start = self.__width
        while start > 0:
            # append blank spaces to the console on each row
            for row in range(total_rows):
                # remove only characters in view
                try:
                    frame = ' ' * self.__frame_size
                    self.draw_text(row, frame)
                except:
                    pass

            start -= self.__frame_size
            self.__window.refresh()
            sleep(self.frame_time)

        self.__window.clear()
        self.__window.refresh()
        sleep(self.frame_time)

    def flip_chars(self, text: str) -> str:
        """Flip characters in a string and return it

        Flippable characters are defined as ASCII characters that
        have a matching horizontally flipped ASCII character (i.e: > <)
        """

        flippable_chars = {
            '(': ')',
            ')': '(',
            '[': ']',
            ']': '[',
            '{': '}',
            '}': '{',
            '\\': '/',
            '/': '\\',
            '>': '<',
            '<': '>'
        }

        new_text = []
        for ch in text:
            if ch in flippable_chars:
                ch = flippable_chars[ch]
            new_text += [ch]

        return ''.join(new_text)
