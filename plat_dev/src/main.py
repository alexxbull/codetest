#!/usr/bin/python3

from os.path import dirname
from random import randint
from sys import argv, exit
from typing import List, Union
from getopt import getopt, GetoptError

try:
    import importlib.resources as pkg_resources
except ImportError:
    # try backported 'importlib_resources' if on Python < 3.7
    import importlib_resources as pkg_resources

try:
    from canvas import Canvas
    import trains
except ModuleNotFoundError:
    # try sl package if program was installed via package system
    from sl.canvas import Canvas
    import sl.trains as trains


def get_train(file_name: str) -> List[str]:
    """Read text file

    Loads train data from a .txt file and returns a List containing each line from the file
    """

    if pkg_resources.is_resource(trains, file_name):
        train = pkg_resources.read_text(trains, f'{file_name}')
        return train.split('\n')
    else:
        exit(f'File "{file_name}" does not exist in {dirname(trains.__file__)}')


def get_random_train() -> List[str]:
    """Return train data from a randomly selected file"""

    train_files = [file for file in pkg_resources.contents(trains) if file.endswith('.txt')]

    if len(train_files) == 0:
        exit(f'No text files found in directory: {dirname(trains.__file__)}')

    train_index = randint(1, len(train_files)-1)
    train = get_train(f'{train_index}.txt')

    return train


def parse_cmd_args(argv: List[str]) -> Union[int, None]:
    """Parse and return valid command line arguments

    Supported commands: -n with int argument
    """

    try:
        opts, _ = getopt(argv, 'n:')
    except GetoptError as e:
        exit(e)

    train_num = None
    for opt, arg in opts:
        if opt == '-n':
            train_num = int(arg)

    return train_num


def select_train() -> List[str]:
    """Return art data

    If -n command line argument is used, returns the data from the corresponding text file
    Else returns a text data from a random file
    """

    train_num = parse_cmd_args(argv[1:])

    if train_num != None:
        return get_train(f'{train_num}.txt')

    return get_random_train()


def main():
    """Print text data loaded from a file to the console"""

    train = select_train()
    canvas = Canvas()

    # draw tracks below train
    total_rows = len(train)
    add_tracks = True if total_rows <= canvas.height else False
    if add_tracks:
        canvas.draw_text(total_rows, '~' * canvas.width)

    # draw train
    canvas.draw(train)

    # remove tracks below train
    if add_tracks:
        canvas.draw_text(total_rows, ' ' * canvas.width)


if __name__ == "__main__":
    main()
