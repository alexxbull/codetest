#!/usr/bin/python3

from setuptools import setup

setup(
    name="sl",
    version="1.0.0",
    author="Alexx Bull",
    author_email="alexxbull@outlook.com",
    description="Displays a long animation of a locomotive crossing the terminal.",
    url="https://gitlab.com/alexxbull/codetest/-/tree/master/plat_dev",
    keywords="sl",
    packages=['sl', 'sl/trains'],
    entry_points={
        'console_scripts': ['sl=sl.main:main'],
    },
    package_data={'sl/trains': ['../trains/*.txt']},
    python_requires='>=3.6',
)
